import pygame
import random

# Inicializar Pygame
pygame.init()

# Configuración de la pantalla
width, height = 800, 600
screen = pygame.display.set_mode((width, height))
pygame.display.set_caption("Esquiva el obstáculo")

# Colores
white = (255, 255, 255)
black = (0, 0, 0)
red = (255, 0, 0)

# Jugador
player_size = 50
player_x = width // 2 - player_size // 2
player_y = height - 2 * player_size
player_speed = 5

# Obstáculo
obstacle_size = 50
obstacle_speed = 5
obstacle_x = random.randint(0, width - obstacle_size)
obstacle_y = -obstacle_size

# Reloj para controlar la velocidad de actualización
clock = pygame.time.Clock()

# Puntuación
score = 0
font = pygame.font.Font(None, 36)

# Función principal del juego
def game():
    global player_x, score, obstacle_x, obstacle_y

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()

        keys = pygame.key.get_pressed()
        player_x += (keys[pygame.K_RIGHT] - keys[pygame.K_LEFT]) * player_speed

        # Actualizar posición del obstáculo
        obstacle_y += obstacle_speed
        if obstacle_y > height:
            obstacle_x = random.randint(0, width - obstacle_size)
            obstacle_y = -obstacle_size
            score += 1

        # Verificar colisión con el jugador
        if (
            player_x < obstacle_x + obstacle_size
            and player_x + player_size > obstacle_x
            and player_y < obstacle_y + obstacle_size
            and player_y + player_size > obstacle_y
        ):
            game_over()

        # Limpiar pantalla
        screen.fill(white)

        # Dibujar jugador
        pygame.draw.rect(screen, red, [player_x, player_y, player_size, player_size])

        # Dibujar obstáculo
        pygame.draw.rect(screen, black, [obstacle_x, obstacle_y, obstacle_size, obstacle_size])

        # Mostrar puntuación
        score_text = font.render("Puntuación: {}".format(score), True, black)
        screen.blit(score_text, (10, 10))

        # Actualizar pantalla
        pygame.display.update()
import pygame
import random

# Inicializar Pygame
pygame.init()

# Configuración de la pantalla
width, height = 800, 600
screen = pygame.display.set_mode((width, height))
pygame.display.set_caption("Esquiva el obstáculo")

# Colores
white = (255, 255, 255)
black = (0, 0, 0)
red = (255, 0, 0)

# Jugador
player_size = 50
player_x = width // 2 - player_size // 2
player_y = height - 2 * player_size
player_speed = 5

# Obstáculo
obstacle_size = 50
obstacle_speed = 5
obstacle_x = random.randint(0, width - obstacle_size)
obstacle_y = -obstacle_size

# Reloj para controlar la velocidad de actualización
clock = pygame.time.Clock()

# Puntuación
score = 0
font = pygame.font.Font(None, 36)

# Función principal del juego
def game():
    global player_x, score, obstacle_x, obstacle_y

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()

        keys = pygame.key.get_pressed()
        player_x += (keys[pygame.K_RIGHT] - keys[pygame.K_LEFT]) * player_speed

        # Actualizar posición del obstáculo
        obstacle_y += obstacle_speed
        if obstacle_y > height:
            obstacle_x = random.randint(0, width - obstacle_size)
            obstacle_y = -obstacle_size
            score += 1

        # Verificar colisión con el jugador
        if (
            player_x < obstacle_x + obstacle_size
            and player_x + player_size > obstacle_x
            and player_y < obstacle_y + obstacle_size
            and player_y + player_size > obstacle_y
        ):
            game_over()

        # Limpiar pantalla
        screen.fill(white)

        # Dibujar jugador
        pygame.draw.rect(screen, red, [player_x, player_y, player_size, player_size])

        # Dibujar obstáculo
        pygame.draw.rect(screen, black, [obstacle_x, obstacle_y, obstacle_size, obstacle_size])

        # Mostrar puntuación
        score_text = font.render("Puntuación: {}".format(score), True, black)
        screen.blit(score_text, (10, 10))

        # Actualizar pantalla
        pygame.display.update()

        # Controlar la velocidad de actualización
        clock.tick(30)

# Función de juego terminado
def game_over():
    global score

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_RETURN:
                    # Reiniciar el juego
                    score = 0
                    game()

        # Limpiar pantalla
        screen.fill(white)

        # Mostrar mensaje de Game Over
        game_over_text = font.render("¡Game Over!", True, black)
        screen.blit(game_over_text, (width // 2 - 100, height // 2 - 50))

        # Mostrar puntuación
        score_text = font.render("Puntuación: {}".format(score), True, black)
        screen.blit(score_text, (width // 2 - 100, height // 2))

        # Mostrar instrucciones de reinicio
        restart_text = font.render("Presiona ENTER para reiniciar", True, black)
        screen.blit(restart_text, (width // 2 - 160, height // 2 + 50))

        # Actualizar pantalla
        pygame.display.update()

        # Controlar la velocidad de actualización
        clock.tick(30)

# Ejecutar el juego
game()
        # Controlar la velocidad de actualización
clock.tick(30)

# Función de juego terminado
def game_over():
            pygame.quit()
            quit()

# Ejecutar el juego
game()
